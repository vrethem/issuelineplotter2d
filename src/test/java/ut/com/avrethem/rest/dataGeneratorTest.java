package ut.com.avrethem.rest;

import org.junit.Test;
import org.junit.After;
import org.junit.Before;

import static org.junit.Assert.*;

import com.avrethem.rest.DataGenerator;
import com.avrethem.rest.DataGeneratorModel;
import javax.ws.rs.core.Response;

public class dataGeneratorTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test
    public void messageIsValid() {
        DataGenerator resource = new DataGenerator();

        Response response = resource.getMessage();
        final DataGeneratorModel message = (DataGeneratorModel) response.getEntity();

        assertEquals("wrong message","Hello World",message.getMessage());
    }
}
