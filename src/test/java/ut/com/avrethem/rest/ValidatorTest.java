package ut.com.avrethem.rest;

import org.junit.Test;
import org.junit.After;
import org.junit.Before;
import org.mockito.Mockito;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import com.avrethem.rest.Validator;
import com.avrethem.rest.ValidatorModel;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.GenericEntity;

public class ValidatorTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test
    public void messageIsValid() {
        Validator resource = new Validator();

        Response response = resource.getMessage();
        final ValidatorModel message = (ValidatorModel) response.getEntity();

        assertEquals("wrong message","Hello World",message.getMessage());
    }
}
