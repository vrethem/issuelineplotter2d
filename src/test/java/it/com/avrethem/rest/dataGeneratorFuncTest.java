package it.com.avrethem.rest;

import org.junit.Test;
import org.junit.After;
import org.junit.Before;

import static org.junit.Assert.*;

import com.avrethem.rest.DataGeneratorModel;
import org.apache.wink.client.Resource;
import org.apache.wink.client.RestClient;

public class dataGeneratorFuncTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test
    public void messageIsValid() {

        String baseUrl = System.getProperty("baseurl");
        String resourceUrl = baseUrl + "/rest/datagenerator/1.0/generateDataset/testmessage";

        RestClient client = new RestClient();
        Resource resource = client.resource(resourceUrl);

        DataGeneratorModel message = resource.get(DataGeneratorModel.class);

        assertEquals("wrong message","Hello World",message.getMessage());
    }
}
