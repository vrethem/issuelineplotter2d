package it.com.avrethem.rest;

import org.junit.Test;
import org.junit.After;
import org.junit.Before;
import org.mockito.Mockito;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import com.avrethem.rest.Validator;
import com.avrethem.rest.ValidatorModel;
import org.apache.wink.client.Resource;
import org.apache.wink.client.RestClient;

public class ValidatorFuncTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test
    public void messageIsValid() {

        String baseUrl = System.getProperty("baseurl");
        String resourceUrl = baseUrl + "/rest/validator/1.0/validate";

        RestClient client = new RestClient();
        Resource resource = client.resource(resourceUrl);

        ValidatorModel message = resource.get(ValidatorModel.class);

        assertEquals("wrong message","Hello World",message.getMessage());
    }
}
