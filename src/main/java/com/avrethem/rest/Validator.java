package com.avrethem.rest;

import com.atlassian.jira.util.lang.JiraStringUtils;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.avrethem.utils.ValidationError;
import com.avrethem.utils.ErrorCollection;
import com.sun.org.apache.xpath.internal.operations.Number;

/**
 * A resource of message.
 */
@Path("/validate")
public class Validator {

    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getMessage()
    {
       return Response.ok(new ValidatorModel("Hello World")).build();
    }

    @Path("/validator")
    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    public Response validate(@QueryParam("projectOrFilterId") String projectOrFilterId,
                             @QueryParam("timeUnit") String timeUnit,
                             @QueryParam("daysPrev") String daysPrev) {
        ErrorCollection errorCollection = new ErrorCollection();
        if (projectOrFilterId.length() == 0) {
            ValidationError validationError = new ValidationError(
                    "projectOrFilterId",
                    "gadget.config.required"
            );
            errorCollection.addError(validationError);
        }
        if (daysPrev.length() == 0) {
            ValidationError validationError = new ValidationError(
                    "daysPrev",
                    "gadget.config.required"
            );
            errorCollection.addError(validationError);
        } else if ( !daysPrev.matches("^[0-9]+$") ) {
            ValidationError validationError = new ValidationError(
                    "daysPrev",
                    "gadget.config.daysprev"
            );
            errorCollection.addError(validationError);
        }
        else {
            if (Integer.parseInt(daysPrev) <= 0) {
                ValidationError validationError = new ValidationError(
                        "daysPrev",
                        "gadget.config.daysprev"
                );
                errorCollection.addError(validationError);
            }

            // Prevent laggy graph
            if (timeUnit.equals("d") && Integer.parseInt(daysPrev) > 2000) {
                ValidationError validationError = new ValidationError(
                        "daysPrev",
                        "gadget.config.daysprev.daylimit"
                );
                errorCollection.addError(validationError);
            } else if (timeUnit.equals("h") && Integer.parseInt(daysPrev) > 100) {
                ValidationError validationError = new ValidationError(
                        "daysPrev",
                        "gadget.config.daysprev.hourlimit"
                );
                errorCollection.addError(validationError);
            } else if (timeUnit.equals("m") && Integer.parseInt(daysPrev) > 5) {
                ValidationError validationError = new ValidationError(
                        "daysPrev",
                        "gadget.config.daysprev.minutelimit"
                );
                errorCollection.addError(validationError);
            }
        }

        if (errorCollection.hasErrors()) {
            return Response.status(400).entity(errorCollection).build();
        }
        return Response.ok().build();
    }
}