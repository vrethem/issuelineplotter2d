package com.avrethem.rest;

import javax.xml.bind.annotation.*;
@XmlRootElement(name = "message")
@XmlAccessorType(XmlAccessType.FIELD)
public class ValidatorModel {

    @XmlElement(name = "value")
    private String message;

    public ValidatorModel() {
    }

    public ValidatorModel(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}