package com.avrethem.rest;

import com.atlassian.jira.component.ComponentAccessor;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;


import com.avrethem.servlet.SearchServlet;
import com.avrethem.utils.IssueMap;
import com.avrethem.utils.JsonBuilder;
import com.avrethem.utils.UtilPair;

import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * A resource of message.
 */
@Path("/generateDataset")
public class DataGenerator {
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(DataGenerator.class);
    private static final String LONG_DATE_FORMAT = "yyyy-MM-dd HH:mm";
    private static final String MEDIUM_DATE_FORMAT = "yyyy-MM-dd HH";
    private static final String SHORT_DATE_FORMAT = "yyyy-MM-dd";

    public String projectOrFilterId = "null";
    public String keyStatus = "null";
    public SimpleDateFormat ldf = null;
    public SimpleDateFormat mdf = null;
    public SimpleDateFormat sdf = null;
    public Date firstDate = null;
    public ApplicationUser user = null;
    public Locale locale = null;
    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/testmessage")
    public Response getMessage()
    {
       return Response.ok(new DataGeneratorModel("Hello World")).build();
    }



    /** How to write doc comments: http://www.oracle.com/technetwork/articles/java/index-137868.html
     *
     *
     * Returns a http-response with a JSON-object with the following format:
     * {issues: Array(N)}
     *  issues:
     *  Array(N)
     *      0: {issuedate: "2018-02-20", total: 8, closed: 3},
     *      1: {issuedate: "2018-02-21", total: 9, closed: 3},
     *      ...
     *      N: {issuedate: "2018-06-21", total: 58, closed: 23}
     * }
     * This is a REST plugin module {@link https://developer.atlassian.com/server/framework/atlassian-sdk/rest-plugin-module/}
     * <p>
     * This method ether return successfull JSON object or serverError
     * This method is used via REST call from a script file or equal (javascript)
     *
     * @param  projectOrFilterIdString  The name (not ID!) of the filter to use in JQL-searches
     * @param  resolution               __UNUSED__ How far back in time the datacollection shall start from
     * @param  firstDateString          The first date that the datacollection should start from
     * @param  searchStatusString       The specific status by name (not ID!) to count
     * @return      A http response with a json object inside
     * @see         Response
     */
    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/asJSON")
    public Response getJSONdataset(@QueryParam("projectOrFilterId") String projectOrFilterIdString,
                                   @QueryParam("timePeriod") String resolution,
                                   @QueryParam("firstDate") String firstDateString,
                                   @QueryParam("statusByName") String searchStatusString)
    {
       /* System.out.println(projectOrFilterIdString);
        System.out.println(timePeriodString);
        System.out.println(firstDateString);
        System.out.println(searchStatusString);*/
        locale = new Locale("sv", "SE");
        user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
        ldf = new SimpleDateFormat(LONG_DATE_FORMAT );
        sdf = new SimpleDateFormat(SHORT_DATE_FORMAT);
        keyStatus = searchStatusString;
        projectOrFilterId = projectOrFilterIdString;

        try {
            firstDateString = firstDateString.substring(0, 10) + " 00:00";
            //projectOrFilterId = filterIdString.split("filter-")[1];
            firstDate = ldf.parse(firstDateString);

            String options = "AND (createdDate > \"" + ldf.format(firstDate) +"\" OR (status CHANGED AFTER \"" + ldf.format(firstDate) + "\"))";
            List<Issue> issues = SearchServlet.getIssues(user, projectOrFilterId, options);

            // Build Accumulative map, each entry has key=date, value=Pair<Open, Closed>
            // How many issues were opened on date, How many issues were closed on date
            //  TreeMap< String date, UtilPair<int open, int closed>>
            IssueMap m = new IssueMap(resolution.charAt(0), locale);

            // Insert values for each issue
            for (Issue issue : issues) {
                insertFromIssue(issue, m);
            }

            //JsonBuilder jsonBuilder = new JsonBuilder(true);
            return JsonBuilder.build(m,this, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.serverError().build();
    }


    public UtilPair getIssueCountBeforeDate(Date date)
    {
        int totBefore = 0;
        int closedBefore = 0;
        try {
            String options = "AND createdDate <= \"" + ldf.format(date) + "\" AND STATUS WAS \"" + keyStatus + "\" ON \"" + ldf.format(date) + "\"";
            List<Issue> issues_closed = SearchServlet.getIssues(user, projectOrFilterId, options);

            closedBefore = issues_closed.size();

            options = "AND createdDate <= \"" + ldf.format(date) + "\" AND STATUS WAS NOT \"" + keyStatus + "\" ON \"" + ldf.format(date) + "\"";
            List<Issue> issues_open = SearchServlet.getIssues(user, projectOrFilterId, options);

            totBefore = issues_open.size() + closedBefore;
        }catch (SearchException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        log.debug("Tot: " + totBefore);
        log.debug("Closed: " + closedBefore);

        return new UtilPair(totBefore, closedBefore);
    }


    public void insertFromIssue(Issue issue, IssueMap m) throws Exception
    {
        //printIssueToLog(issue);
        Date createdDate = ldf.parse(issue.getCreated().toString().substring(0, 16));

        ChangeHistoryManager changeHistoryManager = ComponentAccessor.getChangeHistoryManager();
        List<ChangeItemBean> changeItemBeans = changeHistoryManager.getChangeItemsForField(issue, IssueFieldConstants.STATUS);


        if ( createdDate.after(firstDate) ) {
            // Created AFTER firstDate

            m.incrementTotal(createdDate);

            /* NO-HISTORY */
            if (changeItemBeans.isEmpty()) {
                log.debug("No history for issue in Status : " + issue.getStatus().getName());
                if (keyStatus.equals(issue.getStatus().getName())) {
                    m.incrementClosed(createdDate);
                }
            }
            /* HAS HISTORY */
            else {
                //System.out.print("First history transition\t %-20s\t\t%-12s -> %-12s\n", changeItemBeans.get(0).getCreated().toString().substring(0, 16), changeItemBeans.get(0).getFromString(), changeItemBeans.get(0).getToString());

                // If issue Started in status 'keyStatus'
                if ( changeItemBeans.get(0).getFromString().equals(keyStatus) ) {
                    m.incrementClosed(ldf.parse(changeItemBeans.get(0).getCreated().toString().substring(0, 16)));
                }

                for (ChangeItemBean c : changeItemBeans) {
                    Date beanDate = ldf.parse(c.getCreated().toString().substring(0, 16));
                    insertFromHistoryBeans(c, m, beanDate);
                }
            }
        }
        else {
            // Crated BEFORE firstDate

            // NO-HISTORY
            // Issue must have history

            // HAS HISTORY
            for (ChangeItemBean c : changeItemBeans) {
                Date beanDate = ldf.parse(c.getCreated().toString().substring(0, 16));

                if (beanDate.after(firstDate)) {
                    insertFromHistoryBeans(c, m, beanDate);
                }
            }

        }
    }


    private void printIssueToLog(Issue issue) throws ParseException
    {
        Date createdDate = ldf.parse(issue.getCreated().toString().substring(0, 16));

        log.debug("--------------------------------------------------------------------------------------------------------------------------------------");
        if ( createdDate.after(firstDate) ) {
            log.debug("Found issue created \t      AFTER\t\t\t----- " + issue.getKey() + " -----");
        } else {
            log.debug("Found issue created \t      BEFORE\t\t\t----- " + issue.getKey() + " -----");
        }
        log.debug("Created  \t\t\t " + ldf.format(createdDate));
        log.debug("FirstDate\t\t\t " + ldf.format(firstDate));
        log.debug("Summary : " + ((issue.getSummary() == null) ? "No-summary" : issue.getSummary()) );
        log.debug("--------------------------------------------");
    }
    /*
    *
    *
     */
    private void insertFromHistoryBeans(ChangeItemBean c, IssueMap m, Date beanDate)
    {
        if (c.getToString().equals(keyStatus)) {
            //System.out.printf("<Found Transition>\t %-20s\t\t%-12s -> %-12s\n", ldf.format(beanDate), c.getFromString(), c.getToString());
            m.incrementClosed(beanDate);
        }
        if (c.getFromString().equals(keyStatus)) {
            //System.out.printf("<Found Transition>\t %-20s\t\t%-12s -> %-12s\n", ldf.format(beanDate), c.getFromString(), c.getToString());
            m.decrementClosed(beanDate);
        }
    }

}