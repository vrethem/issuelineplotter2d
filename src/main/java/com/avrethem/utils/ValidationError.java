package com.avrethem.utils;

/**
 * More info: https://developer.atlassian.com/server/framework/gadgets/rest-resource-for-validating-gadget-configuration/
 */
public class ValidationError
{
    public String field;
    public String error;

    public ValidationError(String field, String error) {
        this.field = field;
        this.error = error;
    }
}
