package com.avrethem.utils;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Collection;

/**
 * More info: https://developer.atlassian.com/server/framework/gadgets/rest-resource-for-validating-gadget-configuration/
 */
@XmlRootElement
public class ErrorCollection
{
    /**
     * Generic error messages
     */
    @XmlElement
    private Collection<String> errorMessages = new ArrayList<String>();

    /**
     * Errors specific to a certain field.
     */
    @XmlElement
    private Collection<ValidationError> errors = new ArrayList<ValidationError>();

    public void addError(ValidationError error) {
        this.errors.add(error);
    }

    public boolean hasErrors() {
        return !(errors.isEmpty() && errorMessages.isEmpty());
    }
}
