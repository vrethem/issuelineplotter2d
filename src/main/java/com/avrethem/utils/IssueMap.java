package com.avrethem.utils;

import com.atlassian.jira.datetime.DateTimeFormatUtils;
import com.atlassian.jira.util.JiraDateUtils;
import sun.awt.AWTAccessor;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TreeMap;

public class IssueMap extends TreeMap<String, UtilPair>{
    private int chomp;
    private char resolution;
    public SimpleDateFormat idf;
    public Locale locale;


    public IssueMap(char resolution_id, Locale locale_ ) {
        super();
        setResolution(resolution_id);
        locale = locale_;
    }

    private void setResolution(char resolution_id) {
        resolution = resolution_id;
        if ( resolution_id == "weekly".charAt(0) ) {
            idf = new SimpleDateFormat("YYYY-'W'ww", new Locale("sv", "SE")/*locale*/);
            chomp = 8;
        }
        else {

            if ( resolution_id == "minutely".charAt(0) )
                chomp = "yyyy-MM-dd HH:mm".length();
            else if ( resolution_id == "hourly".charAt(0) )
                chomp = "yyyy-MM-dd HH".length();
            else if ( resolution_id == "daily".charAt(0) )
                chomp = "yyyy-MM-dd".length();
            else if ( resolution_id == "Monthly".charAt(0) )
                chomp = "yyyy-MM".length();
            else
                chomp = "yyyy-MM-dd HH:mm".length();
            idf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS".substring(0, chomp));
        }
    }


    private Date timeUnitOnePlus(Date date) throws ParseException {
        if (resolution == "weekly".charAt(0)) {
            date.setTime(date.getTime() + 1000L * 60L * 60L * 24L * 7); // +1w
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", new Locale("sv", "SE"));
            chomp = "yyyy-MM-dd".length();
            String d = sdf.format(date);
            return sdf.parse(d);
        } else {
            if (resolution == "minutely".charAt(0))
                date.setTime(date.getTime() + 1000L * 60L + 1L); // +1m
            else if (resolution == "hourly".charAt(0))
                date.setTime(date.getTime() + 1000L * 60L * 60L + 1L); // +1H
            else if (resolution == "daily".charAt(0))
                date.setTime(date.getTime() + 1000L * 60L * 60L * 24L + 1L); // +1d
            else if (resolution == "Monthly".charAt(0)) {
                date.setTime(date.getTime() + 1000L * 60L * 60L * 24L * 31L + 1L); // +1M
            }
            return date;
        }
    }


    /**
     * Set date to be one timeUnit++
     */
    public String onePlus(Date date, SimpleDateFormat rdf) throws ParseException {
        int idfLength = idf.toLocalizedPattern().length();
        int fLength = rdf.toLocalizedPattern().length();

        date = timeUnitOnePlus(date);
        // Never send something beyond Now()
        if (date.after(new Date()))
            return rdf.format(new Date());

        return rdf.format(date).substring(0, fLength);
    }


    public UtilPair incrementClosed(Date date) throws ClassCastException, NullPointerException {
        String key = idf.format(date).substring(0, chomp);
        //System.out.println("++ Close isssue  " + date + "  ==  " + key);//   \t[" + key + "]");
        UtilPair value = super.containsKey(key) ? super.get(key) : new UtilPair();
        return super.put(key, value.add(0, 1));
    }

    public UtilPair decrementClosed(Date date) throws ClassCastException, NullPointerException {
        String key = idf.format(date).substring(0, chomp);
        //System.out.println("-- ReOpen isssue + date + "  ==  " + key);//   \t[" + key + "]");
        UtilPair value = super.containsKey(key) ? super.get(key) : new UtilPair();
        return super.put(key, value.sub(0, 1));
    }

    public UtilPair incrementTotal(Date date) throws ClassCastException, NullPointerException {
        String key = idf.format(date).substring(0, chomp);
        //System.out.println("## Add open issue" + date + "  ==  " + key);//   \t[" + key + "]");
        UtilPair value = super.containsKey(key) ? super.get(key) : new UtilPair();
        return super.put(key, value.add(1, 0));
    }


}
