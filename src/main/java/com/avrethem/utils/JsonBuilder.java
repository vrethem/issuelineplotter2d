package com.avrethem.utils;

import com.avrethem.rest.DataGenerator;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.util.*;

import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.avrethem.servlet.SearchServlet;


public class JsonBuilder {


    static public Response build(IssueMap m, DataGenerator dataGenerator, boolean accumulative) throws JSONException, ParseException {
        JSONObject jsonObject = new JSONObject();

        UtilPair count = dataGenerator.getIssueCountBeforeDate(dataGenerator.firstDate);
        buildStartDate(dataGenerator, count, jsonObject);

        buildIssuesArray(dataGenerator, m, count, jsonObject, accumulative);

        return Response.ok(jsonObject.toString(),  MediaType.APPLICATION_JSON).build();
    }


    static private void buildStartDate(DataGenerator dataGenerator, UtilPair startVal, JSONObject jsonObject) throws JSONException {
        JSONObject jsonItem = new JSONObject();

        jsonItem.put("startdate", dataGenerator.ldf.format(dataGenerator.firstDate));
        jsonItem.put("total", startVal.open);
        jsonItem.put("closed", startVal.closed);
        jsonItem.put("projectorfiltername", SearchServlet.getProjectOrFilterByName(dataGenerator.projectOrFilterId, dataGenerator.user));

        jsonObject.put("firstdate", jsonItem);
    }


    static private void buildIssuesArray(DataGenerator dataGenerator, IssueMap m, UtilPair count, JSONObject jsonObject, boolean accumulative) throws JSONException, ParseException {
        JSONArray jsonArray = new JSONArray();
        for(Map.Entry<String, UtilPair> entry : m.entrySet()) {
            if ( accumulative )
                count.add(entry.getValue());

            JSONObject jsonItem = new JSONObject();
            String issuedate = m.onePlus(m.idf.parse(entry.getKey()), dataGenerator.ldf);
            jsonItem.put("issuedate", issuedate);
            jsonItem.put("total", count.open);
            jsonItem.put("closed", count.closed);
            jsonArray.put(jsonItem);
        }
        jsonObject.put("issues", jsonArray);
    }

}
