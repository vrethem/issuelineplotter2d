package com.avrethem.servlet;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;

import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchRequestManager;

public class SearchServlet extends HttpServlet{
    private static final long serialVersionUID = 1L;
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(SearchServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ApplicationUser loggedInUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();

        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();

        writer.println("This is how a servlet module works... <br><br>");
        writer.println("<br>1<br><br>");
        writer.println("<br>2<br>");
        writer.println("<br>3<br><br>");
        writer.println("<br>4<br><br>");
        writer.println("<br>5");
    }


    static public List<Issue> getIssues(ApplicationUser user, String projectOrFilterId, String options ) throws SearchException {
        String jqlString = null;
        if ( projectOrFilterId.contains("filter") ) {
            jqlString = SearchServlet.getQueryStringbyFilter(user, projectOrFilterId);
        }
        else if ( projectOrFilterId.contains("project") ) {
            jqlString = SearchServlet.getQueryStringbyProject(user, projectOrFilterId);
        }

        if ( !jqlString.toLowerCase().contains("date") )
            jqlString += " " + options;

        return getIssuesByQueryString(user, jqlString);
    }

    static private List<Issue> getIssuesByQueryString(ApplicationUser user, String jqlQuery ) throws SearchException {
        SearchService searchService = ComponentAccessor.getComponent(SearchService.class);
        SearchService.ParseResult parseResult = searchService.parseQuery(user, jqlQuery);

        if (parseResult.isValid()) {
            Query query = parseResult.getQuery();
            SearchResults results = searchService.search(user, query, PagerFilter.getUnlimitedFilter());
            log.debug("[line-plotter-2d]@SearchServlet::getIssuesByQueryString(...)]Do Query: '" + jqlQuery + "'");
            return results.getIssues();
        } else {
            log.error(">< >< >< Error >< >< >< parsing query: '" + jqlQuery + "'");
            return Collections.emptyList();
        }
    }

    static private String getQueryStringbyFilter(ApplicationUser user, String filterId ) throws SearchException {
        SearchRequestManager srm = ComponentAccessor.getComponentOfType(SearchRequestManager.class);
        filterId = filterId.split("filter-")[1];
        SearchRequest filter = srm.getSearchRequestById(user, Long.valueOf(filterId));
        //log.debug("getQueryByString: " + filter.getQuery().getQueryString());
        return filter.getQuery().getQueryString();
    }


    static private String getQueryStringbyProject(ApplicationUser user, String projectId ) throws SearchException {
        return "project = " + getProjectOrFilterByName(projectId, user);
    }

    static public String getProjectOrFilterByName(String projectOrFilterId, ApplicationUser user) {
        if ( projectOrFilterId.contains("filter") ) {
            SearchRequestManager srm = ComponentAccessor.getComponentOfType(SearchRequestManager.class);
            projectOrFilterId = projectOrFilterId.split("filter-")[1];
            SearchRequest filter = srm.getSearchRequestById(user, Long.valueOf(projectOrFilterId));
            return filter.getName();
        }
        else if ( projectOrFilterId.contains("project") ) {
            ProjectManager projectManager = ComponentAccessor.getProjectManager();
            projectOrFilterId = projectOrFilterId.split("project-")[1];
            Project project = projectManager.getProjectObj(Long.valueOf(projectOrFilterId));
            return project.getName();
        }
        else
            return "ERROR_FATAL_projectOrFilter unknown";
    }
}