function restRequestsTest() {
    return "test.js loaded";
}


function findNamebyId(arg)
{
    var name = arg["projectOrFilterId"];
    AJS.$.ajax({
    url: "/rest/gadget/1.0/filtersAndProjects?",
    type: 'get',
    dataType: 'json',
    async: false,
    success: function(data) { 
        var jsonOuter = data.options;
        for ( var i=0; i < jsonOuter.length; i++) {
            var jsonInner = jsonOuter[i].group.options;
            for ( var j=0; j < jsonInner.length; j++) {
                if ( jsonInner[j].value == arg["projectOrFilterId"] ) {
                name = jsonInner[j].label;
                return;
                }    
            }
        }
    }
    }); // If unsucessfull return argument arg["projectOrFilterId"]
    return name;
}


function getCurrentUserName()
{
    var user;
    AJS.$.ajax({
    url: "/rest/gadget/1.0/currentUser",
    type: 'get',
    dataType: 'json',
    async: false,
    success: function(data) { user = data["username"]; }
    });
    return user;
}


function getFieldInJasonAtURL()
{
	var result;
	AJS.$.ajax({
	url: AJS.escapeHtml("/rest/api/2/project"), //"/rest/api/2/search?jql=project=%27PROJ%27",
	type: 'get',
    contentType: "application/json",
    dataType: "json",
	async: false,
	success: function(data) { result = data["name"]; }
	});
	return result;
}
