
function reportspluginTest() {
	return "reports-plugin.js loaded";
}

function baseURL() {

}

/**
* Turn back time in parameter date. (use period)
*
* Set date backawards in time depending on param 'num' and gadget.getPref
* @param {Date}    date        Date to set
* @param {int}     num         Number of time units to set date backwards
* 
* @return {type} void
*/
function setDateBackwardsInTime(date, num, unit) {
	if ( unit == "ms" )
		date.setTime(date.getTime() - num);
	else if ( unit == "s" )
		date.setTime(date.getTime() - num*1000);
	else if ( unit == "m" )
		date.setTime(date.getTime() - num*1000*60);
	else if ( unit == "h" )
		date.setTime(date.getTime() - num*1000*60*60);
	else if ( unit == "d" )
		date.setDate(date.getDate() - num);
	else if ( unit == "w" ) {
		setDateBackwardsInTime(date, (num < 0) ? -1:1, 'd');
		while(date.getUTCDay() != 1) 
			setDateBackwardsInTime(date, (num < 0) ? -1:1, 'd');
	}
	else if ( unit == "M" )
		date.setMonth(date.getMonth() - num);
	else 
		date.setYear(date.getFullYear() - num);
}

/**
* Parse json to google line chart
*
* Issues will be sorted in ascending order 
* Loop until date == createdDate for next issue in json
*
* Set date backawards in time depending on param 'num' and gadget.getPref
* @param {Struct}  arg          arguments to function
* 
* @return {type} void
*/
function parseDataToChart(arg) {        
	var jsonArray = arg["json"].issues;
	var jsonStart = arg["json"].firstdate;

	var total =  (arg["countFromZero"] == "true") ? parseInt(jsonStart.total) : jsonStart.total;
	var closed = (arg["countFromZero"] == "true") ? parseInt(jsonStart.closed) : jsonStart.closed;

	var date = new Date(arg["json"].firstdate.startdate);
	date.setTime(date.getTime() - new Date().getTimezoneOffset()*60*1000); 

	var now = new Date();
	now.setTime(now.getTime() - new Date().getTimezoneOffset()*60*1000);

    // Loop in acsending dates
    var i = 0;
    while (  i < jsonArray.length ) 
    {   
    	var issuedate = new Date(jsonArray[i].issuedate);
    	issuedate.setTime( issuedate.getTime() - new Date().getTimezoneOffset()*60*1000 ); 
    	//
    	// Untill iterated till date > issuedate
    	//
    	while ( date < issuedate ) 
    	{ 
    		addRow(arg, date, total, closed);
    		setDateBackwardsInTime(date, -1, arg["timeUnit"]);
    	}

    	//
    	// Insert row form array
    	//
    	total = parseInt(jsonArray[i].total);
    	closed = parseInt(jsonArray[i].closed);
 	
    	addRow(arg, issuedate, total, closed);
    	//date.setTime(issuedate.getTime());
    	setDateBackwardsInTime(date, -1, arg["timeUnit"]);
    	i++;
    }
    setDateBackwardsInTime(date, 1, arg["timeUnit"]);
    //
    // Keep going until date > now()
    //
    console.log("date: " + date);
    console.log("now: " + now);
    if ( date < now )
    { 
    	while ( date < now ) {
    		addRow(arg, date, total, closed);
    		setDateBackwardsInTime(date, -1, arg["timeUnit"]); 		
    	}
    	// Add last data point for Now()
    	addRow(arg, now, total, closed);
    }
    //else if ( date != now )
    //	addRow(arg, now, total, closed);
}

/**
* Add one row to chartData & trendData
* @return {type} void
*/
function addRow(arg, date, total, closed) {
	var jsonArray = arg["json"].issues;
	var jsonStart = arg["json"].firstdate;

	var insertOpen = total;
	var insertClosed = closed;
	if ( arg["countFromZero"] == "true" ) {
		insertOpen -=  parseInt(jsonStart.total);
		insertClosed -= parseInt(jsonStart.closed);
	}
	else {
		insertOpen -= insertClosed;
	}

	var d = new Date(date);
	d.setTime(date.getTime() + new Date().getTimezoneOffset()*60*1000); 

	//console.log(d + " Total: " + total + " | InStatus: " + insertClosed + " | Total :" + insertOpen + " ]");
	if (arg["countFromZero"] == "false")
		arg["chartData"].addRow([d, insertClosed, insertOpen, total]);
	else
		arg["chartData"].addRow([d, insertClosed, insertOpen]);
	arg["trendData"].addRow([d, (total - closed) - (parseInt(jsonStart.total) - parseInt(jsonStart.closed))]);
}




