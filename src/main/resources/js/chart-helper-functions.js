function testTest() {
	return "chart-helper-functions.js loaded";
}


function findMax(arg) {
	var jsonStart = arg["json"].firstdate;
	var jsonArray = arg["json"].issues;

	var max = 0;
	if (arg["countFromZero"] == "true") {
		max = 0;
	} else {
		max = parseInt(jsonStart.total);
	}

	for (var i = 0; i < jsonArray.length > 0; i++) {
		var tmpOpen = parseInt(jsonArray[i].total);
		var tmpClosed = parseInt(jsonArray[i].closed);
		var tmp = 0;
		if (arg["countFromZero"] == "true") {
			tmpOpen -= parseInt(jsonStart.total);
			tmpClosed -= parseInt(jsonStart.closed);
		}
		tmp = (tmpClosed > tmpOpen) ? tmpClosed : tmpOpen;
		if (tmp > max)
			max = tmp;
	}

	return max + 2;
}


function findMin(arg) {
	var jsonStart = arg["json"].firstdate;
	var jsonArray = arg["json"].issues;

	var min = findMax(arg);
	if (arg["countFromZero"] == "true") {
		min = 0;
	} else {
		min = (parseInt(jsonStart.total) < parseInt(jsonStart.closed)) ? parseInt(jsonStart.total) : parseInt(jsonStart.closed);
	}

	for (var i = 0; i < jsonArray.length > 0; i++) {
		var tmpOpen = parseInt(jsonArray[i].total);
		var tmpClosed = parseInt(jsonArray[i].closed);

		if (arg["countFromZero"] == "true") {
			tmpOpen -= parseInt(jsonStart.total);
			tmpClosed -= parseInt(jsonStart.closed);
		} 

		var tmp = (tmpClosed < tmpOpen) ? tmpClosed : tmpOpen;
		if (tmp < min)
			min = tmp;
	}
	return (min >= 2) ? (min - 2) : min;
}


function getMainChartOptions(arg) {
	var d = new Date();
	d.setTime(d.getTime() - new Date().getTimezoneOffset() * 60 * 1000);
	return {
		title: ((arg["countFromZero"] == "false") ? 'All previous issues' : 'Issues in the last ' + arg["daysPrev"] + ' days'),
		//subtitle: 'Issues in the last ' + arg["daysPrev"] + ' days',
		isStacked: (arg["countFromZero"] == "true") ? false : true,
		curveType: 'none',
		focusTarget: (arg["countFromZero"] == "false") ? 'category' : 'category',
		selectionMode: 'none',
		// Trigger tooltips
		tooltip: {
			trigger: 'focus',
			showColorCode: true
		},
		aggregationTarget: 'category',
		explorer: { actions: ['dragToZoom', 'rightClickToReset'] },
		crosshair: { trigger: 'focus', orientation: 'vertical', color: '#505050', opacity: 0.6 },
		seriesType: 'area',
		series: {
			0: {// set options for the Open
				pointsVisible: true,
				color: '#8eb021'
			}, 
			1: {// set options for the Closed
				pointsVisible: true,
				color: '#d04437'
			},
			2: {
				format:'#,###%',
				type: 'line',
				lineWidth: 0,
				pointSize: 0,
				visibleInLegend: false,
				pointsVisible: false,
				color: '#ffffff'
			}
		},
		legend: {
			position: 'top',
			maxlines: 2
		},
		lineWidth: 3,
		hAxis: {
			//title: 'Date'
			gridlines: {
				count: -1,
				units: {
					days: {format: ['MMM dd']},
					hours: {format: ['HH', 'HH']},
				}
			},
			minorGridlines: {
            units: {
              hours: {format: ['HH:mm:ss Z', 'HH']},
              minutes: {format: ['HH:mm a Z', ':mm']}
            }
          }
			/*,
			viewWindow: {
				min: arg["firstDate"], //new Date(arg["firstDate"].getFullYear(), (arg["firstDate"].getMonth()), arg["firstDate"].getDate()),
				max: d
			}*/
		},
		vAxis: {
			//title: 'Issues',
			viewWindow: {
				min: findMin(arg),
				max: findMax(arg)
			},
			gridlines: {
				count: -1
			},
		},
		height: (arg["windowWidth"] < 1000) ? arg["windowWidth"] / 1.5 : arg["windowWidth"] / 3,
		chartArea: {
			left: '7%',
			top: '10%',
			width: '87%',
			height: '78%'
		}

	};
}


function getTrendChartOptions(arg) {
	var d = new Date();
	d.setTime(d.getTime() - new Date().getTimezoneOffset() * 60 * 1000);
	return {
		curveType: 'none',
		focusTarget: 'category',
		legend: {
			position: 'none'
		},
		selectionMode: 'single',
		tooltip: {
			trigger: 'focus',
			showColorCode: true
		},
		explorer: { actions: ['dragToZoom', 'rightClickToReset'] },
		crosshair: { trigger: 'focus', orientation: 'vertical', color: '#505050', opacity: 0.6 },
		seriesType: 'area',
		series: {
			0: {
				type: 'area',
				areaOpacity: 0,
				lineWidth: 3,
				visibleInLegend: false,
				pointsVisible: false,
				color: '#436fae'
			}
		},
		vAxis: {
			format: '#',
			gridlines: {
				count: -1
			}
		},
		hAxis: {
			//title: 'Date'
			/*,
				min: arg["firstDate"], //new Date(firstDate.getFullYear(), firstDate.getMonth(), firstDate.getDate()),
				max: d
			}*/
			gridlines: {
				count: 0
			},
			textStyle: {
				color: '#ffffff'
			}
		},
		vAxis: {

		},
		chartArea: {
			left: '7%',
			top: '4%',
			width: '87%',
			height: '75%'
		}
	};
}