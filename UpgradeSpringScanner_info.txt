---------------------------------------------------------------------------
---------------------------  pom.xml --------------------------------------
<includeExclude>+com.atlassian.jira.plugins.issue.create.*</includeExclude>
solves [ERROR] on startup

--- Atlassian-spring-scanner 2.x from 1.x
# Automatically notices changes to .jar etc.
1) Spring config file change to http://www.atlassian.com/schema/atlassian-scanner/2
2) Change your dependency on atlassian-spring-scanner-annotation to be <scope>provided</scope>
3) Remove your dependency on atlassian-spring-scanner-runtime.
4) Changes <build><plugin>
5) Remove use of @Scanned - this feature is removed. All classes are now scanned.
6) Remove 'auto-imports' attribute from <scan-indexes>.
7) Change to <atlassian.spring.scanner.version>2.1.3</atlassian.spring.scanner.version> in pom.xml

* Changes OSGi version converting from Version 1 to Version 2
* plugins-version="2" in atlassian-plugin.xml 
* Important warning: Atlassian Spring Scanner does not work with maven versions 3.3.1/3.3.3 due to a maven bug: MNG-5787. Use version 3.3.9. 
https://bitbucket.org/atlassian/atlassian-spring-scanner/src


--- [c.a.plugin.util.WaitUntil] Plugins that have yet to be enabled: (2): [com.atlassia
Plugin wait for some dependencies to be added to the system by some other plugin.


--- IMPORTING DEPENDENCIES
    1) Search for dependencies at Maven repository
       https://mvnrepository.com/
    2) Add to pom.xml 
	>atlas-package / >atlas-run

<!-- WIRED TEST RUNNER DEPENDENCIES --> for intergations test classes

...
<version>SNAPSHOT</version>
...
Using a SNAPSHOT dependency requires that the project that you depend on has actually published a SNAPSHOT release, (latest SNAPSHOT-release).



--- @ComponentImport CANNOT be used with @ExportAsService
Wierd SpringScanner error


--- Bundle Exception atlas-package
When your plugin declares a package import, at runtime it must find another OSGi bundle that exports a compatible version (according to the specified version constraints) of the same package. If such a bundle cant be found, the plugin will fail to load.
FELIX package manager
/jira/plugins/servlet/system/console/events


--- Extra parameters
<parallel>true</parallel>

https://developer.atlassian.com/server/framework/atlassian-sdk/amps-build-configuration-reference/


--- Atlassian Deprecated stuff JIRA ~7.2+
FastDev and atlas-cli have been deprecated. Please use Automatic Plugin Reinstallation with QuickReload instead. 
https://developer.atlassian.com/server/framework/atlassian-sdk/automatic-plugin-reinstallation-with-fastdev/
@Scanned annotation


--- plugins-version="2"
These plugins are dynamically deployed on an internal OSGi container
