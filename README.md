#Issue Line Plotter 2D
Simple gadget, displays issues with google graphs. Programmed in Java and JavaScript with Atlassian SDK. 

Posted on Atlassian marketplace, search for 'Issue Line Plotter 2D' or follow [link].
[link]: https://marketplace.atlassian.com/apps/1220000/issue-line-plotter-2d?hosting=server&tab=overview

##Some basic commands for Atlassian SDK
* atlas-run   -- installs this plugin into the product and starts it on localhost
* atlas-debug -- same as atlas-run, but allows a debugger to attach at port 5005
* atlas-cli   -- after atlas-run or atlas-debug, opens a Maven command line window:
                 - 'pi' reinstalls the plugin into the running product instance
* atlas-help  -- prints description for all commands in the SDK
